import os
import re

from wallpaper import Wallpaper

def get_current_wallpapers():
    try:
        with open(f'{os.environ["HOME"]}/.fehbg', 'r') as f:
            lines = f.readlines()
            # First line is a shebang
            cmd = lines[1]
            wallpaper_names = re.findall(r"\'(\S*)\'", cmd)
        return [ Wallpaper(w) for w in wallpaper_names ]
    except FileNotFoundError:
        return []


def set_wallpapers(wallpapers):
    if not wallpapers:
        return
    paths = '" "'.join([ w.filename for w in wallpapers ])
    cmd = f'feh --bg-fill "{paths}"'
    os.system(cmd)
