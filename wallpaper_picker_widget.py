import os
from PyQt5.QtWidgets import *

from wallpaper import Wallpaper

class WallpaperPickerWidget(QWidget):
    def __init__(self, parent=None, screen=None, wallpaper=None):
        QWidget.__init__(self, parent)

        self.__screen = screen
        if not wallpaper:
            self.__wallpaper = Wallpaper()
        else:
            self.__wallpaper = wallpaper

        self.__label = QLabel(f'Wallpaper for {screen}')
        self.__line_edit = QLineEdit(self.__wallpaper.filename)

        self.__file_choose_button = QPushButton('Choose image')

        self.__line_edit.textChanged.connect(self.__validate_filename_slot)

        self.__file_choose_button.clicked.connect(self.__choose_file_slot)

        # Define row with file input
        hbox = QHBoxLayout()
        hbox.addWidget(self.__line_edit)
        hbox.addWidget(self.__file_choose_button)

        # Define widgets and layout for color layer option
        hbox2 = QHBoxLayout()

        self.__color_choose_button = QPushButton('Choose color')
        self.__color_choose_button.clicked.connect(self.__choose_color_slot)
        hbox2.addWidget(self.__color_choose_button)

        hbox2.addWidget(QLabel('Transparency:'))
        self.__transparency_input = QSpinBox()
        self.__transparency_input.setSuffix('%')
        self.__transparency_input.setRange(0, 100)
        self.__transparency_input.textChanged.connect(self.__transparency_input_change_slot)
        hbox2.addWidget(self.__transparency_input)

        hbox2.addWidget(QLabel('Add color layer'))
        self.__add_color_layer_checkbox = QCheckBox()
        hbox2.addWidget(self.__add_color_layer_checkbox)

        vbox = QVBoxLayout()
        vbox.addWidget(self.__label)
        vbox.addLayout(hbox)
        vbox.addLayout(hbox2)

        self.__layout = vbox
        self.setLayout(self.__layout)

    def __validate_filename_slot(self):
        text = self.__line_edit.text()
        text = text.strip()
        if text[:4] == 'file':
            text = text[7:]
        self.__line_edit.setText(text)

    def __transparency_input_change_slot(self):
        self.__wallpaper.edit_mask(transparency=self.__transparency_input.value())

    def __choose_file_slot(self):
        file, _ = QFileDialog.getOpenFileName(self,
                                              'Choose image',
                                              f'{os.environ["HOME"]}',
                                              'Image Files (*.png *.jpg *.bmp)')
        if file:
            self.__line_edit.setText(file)

    def __choose_color_slot(self):
        color = QColorDialog.getColor()
        self.__wallpaper.edit_mask(color=color.name())
        if color:
            self.__color_choose_button.setText(color.name())

    @property
    def wallpaper(self):
        if self.__add_color_layer_checkbox.isChecked():
            self.__wallpaper.apply_mask()
            self.__line_edit.setText(self.__wallpaper.filename)

        self.__wallpaper.filename = self.__line_edit.text()
        return self.__wallpaper
