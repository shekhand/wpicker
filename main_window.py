from PyQt5.QtWidgets import *

from wallpaper_picker_widget import WallpaperPickerWidget
from wallpapers_backend import get_current_wallpapers, set_wallpapers
from xrandr_backend import get_screens

class MainWindowWidget(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.__main_layout = QVBoxLayout()

        self.__wallpapers = get_current_wallpapers()
        screens = get_screens()

        self.__pickers = []
        for i, s in enumerate(screens):
            try:
                w = self.__wallpapers[i]
            except IndexError:
                w = None

            self.__pickers.append(WallpaperPickerWidget(screen=s,
                                                        wallpaper=w))
            self.__main_layout.addWidget(self.__pickers[-1])

        buttons = QHBoxLayout()
        self.__apply_button = QPushButton("Set wallpapers")
        self.__apply_button.clicked.connect(self.__apply_slot)
        buttons.addWidget(self.__apply_button)

        self.__restore_button = QPushButton("Restore")
        self.__restore_button.clicked.connect(self.__restore_slot)
        buttons.addWidget(self.__restore_button)

        self.__main_layout.addLayout(buttons)
        self.setLayout(self.__main_layout)

    def __apply_slot(self):
        ws = [ p.wallpaper for p in self.__pickers ]
        set_wallpapers(ws)

    def __restore_slot(self):
        # TODO fix it
        set_wallpapers(self.__wallpapers)

