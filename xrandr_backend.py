import os
import re

class Screen():
    def __init__(self, name, width, height):
        self.name = name
        self.width = width
        self.height = height

    def __str__(self):
        return f'{self.name} screen, {self.width}x{self.height}'

    def __repr__(self):
        return self.__str__()


def get_screens():
    cmd = 'xrandr --listactivemonitors'
    with os.popen(cmd) as output:
        lines = output.readlines()
        # We don't need first line
        lines = lines[1:]

    # Delete some characters
    tr = str.maketrans({ c: ' ' for c in '\n:+*x/' })
    lines = [ line.translate(tr).split() for line in lines ]

    screens = []
    for line in lines:
        scr = Screen(name=line[1], width=line[2], height=line[4])
        screens.append(scr)

    return screens


if __name__=='__main__':
    screens = get_screens()
    print(screens)
