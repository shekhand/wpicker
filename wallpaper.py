import os
from PIL import Image

WALLPAPERS_PATH = f'{os.environ["HOME"]}/Pictures/wpicker'

class Wallpaper:
    def __init__(self, filename=''):
        self.filename = filename
        self.__mask_params = {
            'color': '#000000',
            'transparency': 0
            }

    def edit_mask(self, color=None, transparency=None):
        if color:
            self.__mask_params['color'] = color
        if transparency:
            self.__mask_params['transparency'] = transparency

    def apply_mask(self):
        if not self.filename:
            return
        image = Image.open(self.filename)
        mask = Image.new(mode='RGBA',
                         size=image.size,
                         color=(0, 0, 0, self.__mask_params['transparency']))
        image.paste(self.__mask_params['color'], (0, 0), mask)
        dir, image_name = os.path.split(self.filename)
        if not os.path.isdir(WALLPAPERS_PATH):
            os.mkdir(WALLPAPERS_PATH)
        self.filename = f'{WALLPAPERS_PATH}/{image_name}'
        image.save(self.filename)
