#!/usr/bin/python3
from PyQt5.QtWidgets import QApplication
from main_window import MainWindowWidget

app = QApplication([])

window = MainWindowWidget()
window.setFixedSize(400, 0)
window.setWindowTitle("Wpicker")
window.show()

app.exec_()
